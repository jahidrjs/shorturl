<?php

/**
 * @author jahid al mamun <rjs.jahid11@gmail.com>
 * @date: 2016-12-14
 * 
 * @copyright  Copyright (C) 2016 rjs. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 *
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @package product
 * @author Jahid Al Mamun
 */
class Common extends CI_Controller {

    /**
     * This is the constructor method
     * @author Jahid Al mamun
     */
    function __construct() {
        parent :: __construct();
        $this->load->model('common_model');
    }

    function index($token = NULL) {
       
        if($token != NULL){
            $token_details = $this->db->select('*')->where('token',$token)->get('short_list')->row();
            if(sizeof($token_details)>0){
                header("Location: $token_details->exit_url");
            }else{
              echo '<h1>Invalid Token </h1>';   
            }
            
            
        } else {
          echo '<h1>No Exit </h1>'; 
        }
    }


}
