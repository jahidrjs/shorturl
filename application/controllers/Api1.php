<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once('./application/libraries/REST_Controller.php');

/**
 * Projects API controller
 *
 * Validation is missing
 */
class Api1 extends REST_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index_post() {

        if ($_POST) {
            $response['status'] = 'ok';
            $response['status_code'] = 200;
            $token = $this->input->post('token');
            $pass = $this->input->post('password');
            if($pass == '658dfa7aasd'){
                if ($token !== '') {
                if ($token == 'urlshort') {
                    $response = $this->url_short($_POST);
                    $response['status'] = 'ok';
                    $response['url'] = 'oxl.cloud';
                    $response['status_code'] = 200;
                }
                
            } else {
                $response['status'] = 'ok';
                $response['status_code'] = 202;
            }
            }else{
                $response['status'] = 'ok';
                $response['status_code'] = 204;
            }
            
        } else {
            $response['status'] = 'ok';
            $response['status_code'] = 201;
        }
//        finaly return response
        $this->response($response);
    }

    /**
     * 
     * @param type $post
     * @return type
     * this method use to retrive first home content data
     * @author jahid al mamun<rjs.jahid11@gmail.com>
     */
    public function url_short($post) {
        $this->load->model('api_model');
        $url = $post['url'];
        $pass = $post['password'];
        $response = $this->api_model->short_url_generate($url,$pass);

        return $response;
    }

    public function home_data($post) {
        $visitor_lng = lngId($post['visitor_lng']);

        $this->load->model('api_model');
        $response = $this->api_model->home_data($visitor_lng);
        return $response;
    }

    public function checkout($post) {
//        return $post;
        $this->load->model('api_model');

        $cartItems = $post['cartItems'];
        $orderInfo = $post['order_info'];

        //first check customer exist or not
        $cus_email = $orderInfo['email'];
        $cus_check = $this->common_model->customer_check($cus_email);
        if ($cus_check) {
            $customer = $cus_check->cusid;
            $customer_ship = $cus_check->shipid;
        } else {
            //first insert customer data 
            $cusdata = array(
                'name' => $orderInfo['name'],
                'phone' => $orderInfo['phone'],
                'email' => $orderInfo['email'],
                'date' => date('Y-m-d'),
            );

            $customer = $this->common_model->save('customers', $cusdata);
            if ($customer) {
                $cusshipdata = array(
                    'name' => $orderInfo['name'],
                    'phone' => $orderInfo['phone'],
                    'address' => $orderInfo['address'],
                    'date' => date('Y-m-d'),
                    'customer_id' => $customer
                );
                $customer_ship = $this->common_model->save('customer_shipping_address', $cusshipdata);
            }
        }

        $data['cart_items'] = $cartItems;




        $order_number = $this->api_model->order_number();

        $order_data = array(
            'order_number' => $order_number,
            'total' => $orderInfo['total_amt'],
            'order_type' => $orderInfo['type'],
            'cust_shipping_id' => $customer_ship,
            'customer_id' => $customer,
            'tax' => '',
            'shipped_on_date' => sqldate($orderInfo['date'], '/', 'm/d/Y'),
            'shipped_on_time' => $orderInfo['time'],
            'created_on' => date('Y-m-d'),
            'order_time' => date("h:i:s"),
            'status' => 'PENDING'
        );

        $data['order_data'] = $order_data;

        $order_id = $this->common_model->save('orders', $order_data);
        $data['order_id'] = $order_id;
        $response['order_id'] = $order_id;
        if ($order_id) {
            $response['order'] = 1;
            $response['order_number'] = $order_number;

            foreach ($cartItems as $item) {

                $orderitem = array(
                    'order_id' => $order_id,
                    'product_id' => $item['id'],
                    'product_name' => $item['name'],
                    'product_image' => $item['image'],
                    'product_price' => $item['price'],
                    'dptp' => item_dptp($item['id']),
                    'sale_price' => $item['price'],
                    'quantity' => $item['qty'],
                    'delicacy_id' => $item['delicacy_id'],
                    'delicacy_lavel' => $item['delicacy'],
                    'delicacy_name' => $item['delicacy_name'],
                    'delicacy_image' => $item['delicacyimage'],
                    'created_on' => date('Y-m-d')
                );

                if ($this->common_model->save('order_items', $orderitem)) {
                    $response['item_save'] = 1;
                } else {
                    $response['item_save'] = 0;
                }
            }

            $response['order_store'] = 1;
        } else {
            $response['order'] = 0;
            $response['order_store'] = 0;
        }
        //First prepare and check store order data
        //then order Items
        //then send sms with order number
        $delivery_day = $orderInfo['date'] . ' at ' . $orderInfo['time'];
        //send sms on Customer with her order details
        $sms_body = "Your Order `$order_number` has been placed and will be delivered on " . $delivery_day . " Call: 60126480357 for assistance";
        $visitor_phone = $orderInfo['phone'];
        // sms send process start
        $response['sms_phone'] = $visitor_phone;

        //Send SMS on Restaurant with order details
        $restaurant_phone = '60126480357';
        $res_sms_body = $visitor_phone . " Order No: `$order_number` .Delivered on " . $delivery_day . "Note: please check order details with customer Mobile Number.";
        $response['rest_sms'] = send_sms($restaurant_phone, $res_sms_body);

        // sms send process end
        return $response;

        //now send emergency alert mail
        $to = "jahid.bitfountain@gmail.com";
        $subject = "New Order `$order_number` has been placed and Need to be delivered";

        $message = "
               <html>
               <head>
               <title>Your Order `$order_number` has been placed and will be delivered on </title>
               </head>
               <body>
               <h3>Dear Admin!</h3>
               <p><b>Your has been placed and will be delivered on.</b></p>
               
               </body>
               </html>
               ";

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: <info@test.com>' . "\r\n";
        $headers .= 'Cc: jahid@bitfountain.com' . "\r\n";

        mail($to, $subject, $message, $headers);

//        if($customer_email !=''){
//        //Email sending functionality
//        //first prepare email body content html
//        //$email_body = $this->load->view('order_email',$data,true);
//        $email_body = 'jahid';
//        $subject = "eKidMart invoice $order_number";
//        $recipient = $customer_email;
//        
//        $this->load->library('common_library');
//        $response['email_feedback'] = $this->common_library->send_mail($recipient, $subject, $email_body);
//    
//        }
    }

    public function user_exist($post) {

        $this->load->model('api_model');

        $phone = $post['phone'];
        $status = $this->api_model->user_check($phone);
        if ($status) {
            $response['exist'] = 1;
            $response['name'] = $status->cname;
            $response['phone'] = $status->cphone;
            $response['address'] = $status->saddress;
        } else {
            $response['exist'] = 0;
        }
        return $response;
    }

}
