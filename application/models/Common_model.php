<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
 */
class Common_model extends CI_Model {

    /**
     * this method use for all data retrive with table name
     * @author Jahid All Mamun
     */
    public function getall($table) {
        $this->db->from("$table");
        return $this->db->get()->result();
    }
    
    public function getall_doctor($table) {
        $this->db->from("$table")->where('type',7);
        return $this->db->get()->result();
    }
    
    /**
     * This function use for get all data with check status = 1
     * @author Jahid Al Mamun<rjs.jahid11@gmail.com>
     * @param type $table
     * @return type
     */
    public function getall_with_status($table) {
        $this->db->where('status',0);
        $this->db->from("$table");
        return $this->db->get()->result();
    }
    

    
    public function getwhere($table,$condition)
    {
        $text = "select *from ".$table." where ".$condition;
        $query = $this->db->query("$text");
        $result = $query->result();
        return $result;
    }

    /**
     * this method use for one data or rows retrive with table name and id
     * @author Jahid All Mamun
     */
    public function getone($table, $id) {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $this->db->from("$table");
        return $this->db->get()->result();
    }
    public function getrow($table, $id) {
        $this->db->where('id', $id);
        $this->db->limit(1);
        $this->db->from("$table");
        return $this->db->get()->row();
    }
    


    /**
     * delete row with table name and id
     * @author Jahid All Mamun
     */
    public function delete($table, $id, $primary=Null) {
        if($primary!=NULL){$this->db->where($primary, $id);}else{$this->db->where('id', $id);}
        
        $resul = $this->db->delete($table);
        if ($resul) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This method use for save single row with table name and id
     * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
     * @param array value save with table name provided
     */
    public function save($table, $data) {
        $result = $this->db->insert($table, $data);
        if ($result) {
            return $this->db->insert_id();
        } else {
            return FALSE;
        }
    }
   
    /**
     * update table value with table name / id / and array value
     * @author Jahid All Mamun
     */
    function update($table, $id, $data) {
        $this->db->where('id', $id);
        $result = $this->db->update($table, $data);
        if($result)
        {
            return TRUE;
        }  else {
            return FALSE;   
         }
    }

    //########### update for default ##################

    function update_default($table, $id, $data) {
        $this->db->where('id', $id);
        $result = $this->db->update($table, $data);
        if($result)
        {
            return TRUE;
        }  else {
            return FALSE;
        }
    }

    /**
     * get all by id
     * @author jahid al mamun
     */
    public function getallbyid($table, $id) {
        $this->db->where('id', $id);
        $this->db->from("$table");
        return $this->db->get()->result();
    }
    
    public function get_all_with_custome_id($table,$con, $id) {
        $this->db->where($con, $id);
        $this->db->from("$table");
        return $this->db->get()->result();
    }
    
    public function getallbycolumn($table,$column,$value) {
        $this->db->where($column, $value);
        $this->db->from("$table");
        return $this->db->get()->result();
    }

	
    function blank_replace_all($text) { 
    $text = strtolower(htmlentities($text)); 
    $text = str_replace(get_html_translation_table(), "_", $text);
    $text = str_replace(" ", "_", $text);
    $text = preg_replace("/[-]+/i", "_", $text);
    return $text;
}

}
