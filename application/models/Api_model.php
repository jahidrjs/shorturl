<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author Jahid Al Mamun <rjs.jahid11@gmail.com>
 */
class Api_model extends CI_Model {

    function short_url_generate($url,$pass) {
        //first generate unique token
        $token['token'] = uniqid();
        //then save token with url in database
        $data = array(
            'exit_url'=>$url,
            'token'=>$token['token'],
            'date'=>date('Y-m-d'),
            'password'=>$pass
        );
        
        $this->db->insert('short_list',$data);

        return $token;
    }



}
