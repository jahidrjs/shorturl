
<!-----------------start sidebar----------------->
<div class="main-container ace-save-state" id="main-container">
    <script type="text/javascript">
        try {
            ace.settings.loadState('main-container')
        } catch (e) {
        }
    </script>

    <div id="sidebar" class="sidebar responsive ace-save-state">
        <script type="text/javascript">
            try {
                ace.settings.loadState('sidebar')} catch (e) {
            }
        </script>
        <ul class="nav nav-list">
            <li class="">
                <a href="#Dashboard">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>
                <b class="arrow"></b>
            </li>
            <!--------------------start usermanagement-------------------->
            <li class="">
                <a href="#user_management" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text">User Management</span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="">
                        <a href="#admin">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Admin User
                        </a>
                        <b class="arrow"></b>
                    </li>

                    <li class="">
                        <a href="#customer">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Customer
                        </a>

                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
            <!--end user management-->

            <!----------product--------->
            <li class="">
                <a href="" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text">Product</span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="">
                        <a href="#product">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Product
                        </a>

                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="#product_color">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Product color
                        </a>

                        <b class="arrow"></b>
                    </li>

                    <li class="">
                        <a href="#product_size">
                            <i class="menu-icon fa fa-caret-right"></i>
                            Product size
                        </a>

                        <b class="arrow"></b>
                    </li>
                </ul>
            </li>
            <!---------------end product------>
            <!---------------------category------------->
            <li class="">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-list"></i>
                    <span class="menu-text">Category</span>

                    <b class="arrow fa fa-angle-down"></b>
                </a>

                <b class="arrow"></b>

                <ul class="submenu">
                    <li class="">
                        <a href="#category">
                            <i class="menu-icon fa fa-caret-right"></i>
                             Categories
                        </a>
                        <b class="arrow"></b>
                    </li>
                    <li class="">
                        <a href="#category_menu">
                            <i class="menu-icon fa fa-caret-right"></i>
                           Category menu assign
                        </a>
                        <b class="arrow"></b>
                    </li>

                </ul>
            </li>
            <!--------------------category end----->
            <!------------coupon management--------->
            <li class="">
                <a href="#coupon">
                    <i class="menu-icon fa fa-list-alt"></i>
                    <span class="menu-text">Coupon</span>
                </a>

                <b class="arrow"></b>
            </li>
            <!---------------end coupon------------>
            <li class="">
                <a href="#page">
                    <i class="menu-icon fa fa-list-alt"></i>
                    <span class="menu-text"> Page Management</span>
                </a>

                <b class="arrow"></b>
            </li>
            <!-----------brand start---------->
            <li class="">
                <a href="#brand">
                    <i class="menu-icon fa fa-list-alt"></i>
                    <span class="menu-text"> Brand Management</span>
                </a>

                <b class="arrow"></b>
            </li>
            <!----------end brand---------->
            <!----------subliar-------->
            <li class="">
                <a href="#supplier">
                    <i class="menu-icon fa fa-calendar"></i>

                    <span class="menu-text">
                        Supplier
                    </span>
                </a>

                <b class="arrow"></b>
            </li>
            <!------------end subliar---------->

            <!----site cofiguretion------->
            <li class="">
                <a href="#site_configuration">
                    <i class="menu-icon fa fa-list-alt"></i>
                    <span class="menu-text"> Site Configuration</span>
                </a>

                <b class="arrow"></b>
            </li>
            <!--------->
            <!-----------language------>
            <li class="">
                <a href="#language">
                    <i class="menu-icon fa fa-list-alt"></i>
                    <span class="menu-text"> Language</span>
                </a>

                <b class="arrow"></b>
            </li>
            <!---------->
            <li class="">
                <a href="#payment">
                    <i class="menu-icon fa fa-list-alt"></i>
                    <span class="menu-text"> Payment</span>
                </a>

                <b class="arrow"></b>
            </li>
            <!-------------->
            <li class="">
                <a href="#promotion">
                    <i class="menu-icon fa fa-list-alt"></i>
                    <span class="menu-text"> Promotion</span>
                </a>

                <b class="arrow"></b>
            </li>
            <!-------------->
            <li class="">
                <a href="#slideshow">
                    <i class="menu-icon fa fa-list-alt"></i>
                    <span class="menu-text"> Slideshow manage </span>
                </a>

                <b class="arrow"></b>
            </li>
            <!------------>


        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
            <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
    </div>
    <!-------------------end sidebar--------------->
    
