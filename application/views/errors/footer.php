
<div class="footer">
    <div class="footer-inner">
        <div class="footer-content">
            <span class="bigger-120">
                <span class="blue bolder">Ekid Mart © 2016</span>
            </span>
        </div>
    </div>
</div>

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
    <i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>
</div><!-- /.main-container -->



    <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<!--angularjs external link -->
 <script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-animate.js"></script>
    <script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-resource.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-route.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-touch.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-sanitize.min.js"></script>
    
    
    
    <!--<script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-sanitize.min.js.map"></script>-->
    <!--<script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-scenario.js"></script>-->
    <script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-parse-ext.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-mocks.js"></script>
    <script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-messages.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-aria.min.js"></script>
    

   <!--<script src="<?php echo base_url(); ?>assets/angular-1.5.8/angular-material.js"></script>-->
   <!--<script src="<?php echo base_url(); ?>ng-app/controllers/matrial_modal.js"></script>-->
 
    <!-- Placed at the end of the document so the pages load faster -->
    
    
   <script src="<?php echo base_url(); ?>ng-app/app.js"></script>
   <!--angularjs external link -->
   
<!-- page specific plugin scripts -->

<!-- ace scripts -->
<script src="<?php echo base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ace.min.js"></script>

<!-- inline scripts related to this page -->
    <!-- boolistbox plugin scripts -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.bootstrap-duallistbox.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.raty.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-multiselect.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery-typeahead.js"></script>
    <!-----------end list script--------------->
    <!-----------jquery DataTable------------------->
    <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/buttons.print.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/buttons.colVis.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.select.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-colorpicker.min.js"></script>
    <!--------------end dataTable---------------------->


    <!-- Text editor plugin scripts -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.custom.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/markdown.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-markdown.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.hotkeys.index.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-wysiwyg.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootbox.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/select2.min.js"></script>
    <!--------------------end--------------------->
    <!----------------------------------nestable list------------->
    <script src="<?php echo base_url(); ?>assets/js/jquery.nestable.min.js"></script>
    <!--------------------------------------end nestable list------------>
